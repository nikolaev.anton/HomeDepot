import pandas as pd
from gensim.models import Word2Vec

res_path = u'../resources/'

# attributes = pd.read_csv(res_path + u'attributes.csv')
# df_train = pd.read_csv(res_path + u'train.csv')
# df_test = pd.read_csv(res_path + u'test.csv')
descriptions = pd.read_csv(res_path + u'product_descriptions.csv')
part_descr = descriptions[0:(len(descriptions)/5)]

model = Word2Vec(sentences=descriptions[u'product_description'], size=100, window=5, min_count=5)
model.save(res_path + u'w2vModel.txt')
